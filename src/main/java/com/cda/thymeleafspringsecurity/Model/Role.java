package com.cda.thymeleafspringsecurity.Model;

public enum Role {
    USER,
    ADMIN
}
